#!/bin/bash
#Choisir une interface réseau

DISPLAY=()
INTERFACES=$(ip l | grep -E '[a-z].*: ' | cut -d ':' -f2 | cut -d ' ' -f2) #On isole les interfaces dans une variable $INTERFACES
set $INTERFACES #Chaque interface isolée par la variable $INTERFACES est inscrite dans un argument

for i in $@ #On cherche la variable $i représentant une interface dans la liste des arguments
do
        IP=$(ip a | grep -E "$i$" | cut -d ' ' -f6) #On isole l'IP de l'interface sur laquelle on boucle
        DISPLAY+=("$i" "$IP") #Dans ce cas, on affiche l'interface et son IP mises en page pour Whiptail, avec un \ à la fin de la ligne
done

IFACE=$(whiptail --title "Interfaces réseau d'entrée" --menu "Choisir l'interface réseau d'entrée " 15 60 4 \
        "${DISPLAY[@]}" 3>&1 1>&2 2>&3)

exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "Vous avez choisi l'interface : " $IFACE
else
    echo "vous avez annulé"
fi